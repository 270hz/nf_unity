﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour
{
    public GameObject Title;
    public GameObject Company;
    // Start is called before the first frame update
    void Start()
    {
        Title.SetActive(true);
        Company.SetActive(false);

        Invoke("ShowCompany", 1);
    }

    void ShowCompany()
    {
        Title.SetActive(false);
        Company.SetActive(true);


        Invoke("LoadMainScene", 1);
    }

    void LoadMainScene()
    {
        Company.SetActive(false);
        SceneManager.LoadScene("main");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
