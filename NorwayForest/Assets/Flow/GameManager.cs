﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject[] Scene;
    
    int nSceneNumber = 0;


    void Start()
    {
        foreach (GameObject go in Scene)
        {
            go.SetActive(false);
        }

        StartScene(nSceneNumber);
    }

    void StartScene(int index)
    {
        //FadeIn.self.StartFadeInOut();

        if (index != 0)
        {
            Scene[index-1].SetActive(false);
        }

        Scene[index].SetActive(true);
    }


    void Update()
    {
        if (Input.GetKeyUp(KeyCode.N)) 
        {
            if (nSceneNumber != Scene.Length -1)
            {
                nSceneNumber += 1;

                StartScene(nSceneNumber);
            }
        }
    }
}
