﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour
{
    public static FadeIn self;

    public float FadeTime = 1f; // Fade효과 재생시간

    Image fadeImg;

    float start;

    float end;

    float time = 0f;

    bool isPlaying = false;

    public delegate void FadeInOutCallback(string str);
    FadeInOutCallback callback;

    void Awake()

    {
        self = this;

        fadeImg = GetComponent<Image>();
        callback = WriteString;
    }

    public void StartFadeInOut()
    {
        StartFadeIn();
    }

    void WriteString(string str)
    {
        Debug.Log(str);

        if (str =="fadeIn")
        {
            StartFadeOut();
        }
    }

    public void StartFadeOut()

    {

        if (isPlaying == true) //중복재생방지

        {

            return;

        }

        start = 1f;

        end = 0f;

        StartCoroutine("FadeOutAnimation");    //코루틴 실행

    }

    public void StartFadeIn()

    {
        
        if (isPlaying == true) //중복재생방지

        {

            return;

        }

        start = 0f;

        end = 1f;


        StartCoroutine("FadeInAnimation");    //코루틴 실행
    }

    IEnumerator FadeOutAnimation()

    {

        isPlaying = true;



        Color fadecolor = fadeImg.color;

        time = 0f;

        fadecolor.a = Mathf.Lerp(start, end, time);



        while (fadecolor.a > 0f)

        {

            time += Time.deltaTime / FadeTime;

            fadecolor.a = Mathf.Lerp(start, end, time);

            fadeImg.color = fadecolor;

            yield return null;

        }

        isPlaying = false;
        callback("fadeOut");

    }


    IEnumerator FadeInAnimation()

    {

        isPlaying = true;

        

        Color fadecolor = fadeImg.color;
        fadecolor.a = start;
        fadeImg.color = fadecolor;

        time = 0f;

        fadecolor.a = Mathf.Lerp(start, end, time);



        while (fadecolor.a < 1f)

        {

            time += Time.deltaTime / FadeTime;

            fadecolor.a = Mathf.Lerp(start, end, time);

            fadeImg.color = fadecolor;

            yield return null;

        }

        isPlaying = false;
        callback("fadeIn");


    }
}