﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveCtrl : MonoBehaviour
{
    private Ray ray;
    private RaycastHit hit;
    private Vector3 movePos = Vector3.zero;

    private Transform tr;
    private Animator anim;

    private NavMeshAgent nv;

    //https://mrbinggrae.tistory.com/120

    // Start is called before the first frame update
    void Start()
    {
        tr = GetComponent<Transform>();
        //Animator = GetComponent<Animator>();
        nv = GetComponent<NavMeshAgent>();
    }


    // Update is called once per frame
    void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 100.0F, Color.green);

        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 100.0f, 1 << 8))
        {
            movePos = hit.point;
            Debug.Log(movePos);
            nv.SetDestination(movePos);
            nv.isStopped = false;
            
        }
       
        if ((tr.position - movePos).sqrMagnitude >= 2.0f * 2.0f)
            {
            Quaternion rot = Quaternion.LookRotation(movePos - tr.position);
            tr.rotation = Quaternion.Slerp(tr.rotation, rot, Time.deltaTime * 8.0f);
            tr.Translate(Vector3.forward * Time.deltaTime * 3.0f);
        }
    }
    


        /*
    public Transform target;
    Vector3 destination;
    NavMeshAgent agent;

    void Start()
    {
        // Cache agent component and destination
        agent = GetComponent<NavMeshAgent>();
        destination = agent.destination;
        //target = GetComponent<Transform>();
    }

    void Update()
    {
        // Update destination if the target moves one unit
        if (Vector3.Distance(destination, target.position) > 1.0f)
        {
            destination = target.position;
            agent.destination = destination;
        }
    }
    */

}
